<?php
class ChamadaController extends ApiAppController {

	public $uses = array('Admin.Chamada');
	
	public function add() {
		
		$data = json_decode(json_encode($this->request->input('json_decode')),true);
		
		$data['atendente_id'] = $this->Auth->user('id');
		
		$this->Chamada->save($data);
		
		$this->set('data', $data );
		$this->set('_serialize', array( 'data' ) );
		
		$this->render(false);
		
	}
}

